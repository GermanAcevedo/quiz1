﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CsvHelper;
using Microsoft.Win32;



namespace Quiz1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string dataFile = @"..\..\myData.txt";
        List<Trip> tripList = new List<Trip>();


        public MainWindow()
        {
            InitializeComponent();
            LoadFile();

        }

        private void LoadFile()
        {
            try
            {

                using (StreamReader sr = new StreamReader(dataFile))
                {
                    string line = sr.ReadLine();

                    while (line != null)
                    {
                        String[] travel = line.Split(';');


                        string destination = travel[0];
                        string name = travel[1];
                        string passport = travel[2];
                        string departure = travel[3];
                        string returnd = travel[4];



                        Trip t = new Trip(destination,name,passport,departure,returnd);

                        tripList.Add(t);

                        line = sr.ReadLine();
                    }
                }
            }
            catch (Exception e)
            {
                // Let the user know what went wrong.

                MessageBox.Show(e.Message, "The file could no be read ", MessageBoxButton.OKCancel, MessageBoxImage.Error);
            }


            LvTravel.ItemsSource = tripList;
        }


        private void BtnSaveSelected_Click(object sender, RoutedEventArgs e)
        {
            //List<Trip> tripsSelectedList = new List<Trip>();
            //foreach (ListViewItem item in LvTravel.SelectedItems.)
            //{
            //    Trip tripselected = new Trip(item.ToString());
            //        tripsSelectedList.Add(tripselected);
            //}

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "TRIP file (*.trips)|*.trips";
            saveFileDialog.Title = "Export to file";
            saveFileDialog.AddExtension = true;
            saveFileDialog.ShowDialog();

            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    using (var writer = new StreamWriter(saveFileDialog.FileName))
                    {
                        using (var trip = new CsvWriter(writer, CultureInfo.InvariantCulture))
                        {
                            trip.Configuration.Delimiter = ",";
                            trip.WriteRecords(tripList);
                            //trip.WriteRecords(tripsSelectedList);
                        }
                    }

                }
                catch (IOException exception)
                {
                    MessageBox.Show("Error writing file: " + exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }





        }

        private void ResetValue()
        {
            LvTravel.Items.Refresh();
            TxtName.Clear();
            TxtDestination.Clear();
            TxtPassport.Clear();
            
            LvTravel.SelectedIndex = -1;
            BtnDeleteTrip.IsEnabled = false;
            BtnUpdateTrip.IsEnabled = false;
        }


        private void BtnAddTrip_Click(object sender, RoutedEventArgs e)
        {
            if ((TxtDestination.Text == ""))
            {
                MessageBox.Show("Input string error.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            else if (TxtName.Text == "")
            {
                MessageBox.Show("Input string error.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            else if (TxtPassport.Text == "")
            {
                MessageBox.Show("Input string error.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            string destination = TxtDestination.Text;
            string name = TxtName.Text;
            string passport = TxtPassport.Text;




            DateTime? departureDate = DtpDeparture.SelectedDate;
            if (departureDate == null)
            {
                MessageBox.Show("Choose a date ", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DateTime? returnDate = DtpReturn.SelectedDate;
            if (returnDate == null)
            {
                MessageBox.Show("Choose a date ", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Trip trip = new Trip(destination, name, passport, departureDate.ToString(), returnDate.ToString());
            tripList.Add(trip);
            ResetValue();

        }

        private void BtnDeleteTrip_Click(object sender, RoutedEventArgs e)
        {
            if (LvTravel.SelectedIndex == -1)
            {
                MessageBox.Show("You need to select one item", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            

            
            
            Trip tripToBeDeleted = (Trip)LvTravel.SelectedItem;
            
            if (tripToBeDeleted != null)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure to delete the trip?", "CONFIRMATION", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    tripList.Remove(tripToBeDeleted);
                    ResetValue();
                }
            }

        }

        private void BtnUpdateTrip_Click(object sender, RoutedEventArgs e)
        {
            if (LvTravel.SelectedIndex == -1)
            {
                MessageBox.Show("You need to select one item", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
           

           
            else if (TxtName.Text == "")
            {
                MessageBox.Show("Input string error.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            else if (TxtPassport.Text == "")
            {
                MessageBox.Show("Input string error.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Trip triptoBeUpdated = (Trip)LvTravel.SelectedItem;
            triptoBeUpdated.Destination = TxtDestination.Text;
            triptoBeUpdated.Name = TxtName.Text;
            triptoBeUpdated.Passport = TxtPassport.Text;
            triptoBeUpdated.DepartDate = DtpDeparture.Text;
            triptoBeUpdated.ReturnDate = DtpReturn.Text;

            ResetValue();

        }

        private void LvTravel_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BtnDeleteTrip.IsEnabled = true;
            BtnUpdateTrip.IsEnabled = true;

            var selectedItem = LvTravel.SelectedItem;
            if (selectedItem is Trip)
            {
                Trip trip = (Trip)LvTravel.SelectedItem;
                TxtName.Text = trip.Name;
                TxtDestination.Text = trip.Destination;
                TxtPassport.Text = trip.Passport;
                DtpDeparture.SelectedDate = DateTime.Parse(trip.DepartDate);
                DtpReturn.SelectedDate = DateTime.Parse(trip.ReturnDate);
                
            }

        }

        private void SaveFile()
        {
            using (StreamWriter writer = new StreamWriter(dataFile))
            {
                foreach (Trip tr in tripList)
                {
                    writer.WriteLine(tr.ToDataString());
                }
            }
        }


        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            SaveFile();
        }
    }
}
