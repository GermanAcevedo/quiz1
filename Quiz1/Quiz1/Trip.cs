﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz1
{
    class Trip
    {
        private string _destination;

        public string Destination
        {
            get { return _destination; }
            set
            {
                string pattern = @"^[A-Za-z 0-9,\-.]{1,50}$";
                Regex rg = new Regex(pattern);
                if (!rg.IsMatch(value))
                {

                    throw new InvalidDataException("Invalid destination");
                }

                _destination = value;

            }


        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                string pattern = @"^[A-Za-z 0-9,\-.]{1,50}$";
                Regex rg = new Regex(pattern);
                if (!rg.IsMatch(value))
                {

                    throw new InvalidDataException("Invalid name");
                }

                _name = value;

            }


        }

        private string _passport;
        public string Passport
        {
            get
            {
                return _passport;
            }
            set
            {
                string pattern = @"^[A-Z 0-9]{9}$";
                Regex rg = new Regex(pattern);
                if (!rg.IsMatch(value))
                {
                    
                    throw new InvalidDataException("Code must be exactly 2 uppercase and 7 numbers");
                }
                _passport = value;

            }
        }

        private string _departDate;
        public string DepartDate
        {
            get
            {
                return _departDate;
            }
            set
            {
                
                _departDate=value;

            }
        }

        private string _returnDate;
        public string ReturnDate
        {
            get
            {
                return _returnDate;
            }
            set
            {

                _returnDate = value;

            }
        }

        public Trip(string destination, string name, string passport, string departDate, string returnDate)
        {
            _destination = destination;
            _name = name;
            _passport = passport;
            _departDate = departDate;
            _returnDate = returnDate;
        }

        public Trip(string dataLine)
        {
            string[] data = dataLine.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidDataException("Line has invalid number for fields:\n" + dataLine);
            }
            
            Destination= data[0];
            Name = data[1];
            Passport = data[2];
            DepartDate = data[3];
            ReturnDate = data[4];

            _destination = Destination;
            _name = Name;
            _passport = Passport;
            _departDate = DepartDate;
            _returnDate = ReturnDate;

        }





        public override string ToString()
        {
            return string.Format("{0} by {1} / {2}, {3}, {4}", Destination, Name, Passport, DepartDate, ReturnDate);
        }

        public string ToDataString()
        {
            return string.Format("{0};{1};{2};{3};{4}", Destination, Name, Passport, DepartDate, ReturnDate);
        }


    }
}
